install:
	pip install -r requirements.txt

runserver:
	python manage.py runserver


makemigrations:
	python manage.py makemigrations

migrate:
	python manage.py migrate
