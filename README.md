# Open Stuff Sharing (OSS)



## Aufsetzen einer Testinstanz

Es wird eine aktuell supportete Version von Python benötigt (am besten größer oder gleich 3.6).
Um das Projekt nicht direkt in die Libraries vom Host-System zu installieren wird empfohlen, eine virtuelle Umgebung einzurichten und zu starten.
Am Beispiel von `virtualenv` sieht das folgendermaßen aus:

	$ sudo apt install virtualenv
	$ virtualenv -p /usr/bin/python3 ~/.virtualenvs/my_virtualenv
	$ source ~/.virtualenvs/my_virtualenv/bin/activate

Nun ins Projektverzeichnis wechseln und alle Abhängigkeiten installieren:

	$ make install

Dann werden die Migrationen durchgeführt und anschließend kann der Entwicklungsserver gestartet werden:

	$ make migrate
	$ make runserver

Nun kann im Browser über `localhost:8000` auf die Webseite zugegriffen werden.

Zu Beginn gibt es noch keinen Admin-User. Dieser wird wie folgt angelegt:

	$ python manage.py createsuperuser

Anschließend kann man sich unter dem URL `localhost:8000/admin` in der Admin-Oberfläche anmelden und ggf. weitere Nutzer anlegen.



## Verwenden einer anderen Datenbank

Für die Entwicklung ist eine sqlite Datenbank im Einsatz.
Da diese keine Parallelen Transaktionen ermöglichst ist diese für einen produktiven Einsatz nicht geeignet.
Mit wenigen Schritten kann aber ein anderes Datenbankmanagementsystem angebunden werden.
Am Beispiel von `PostgreSQL` sieht das folgendermaßen aus:

	$ pip install psycopg2-binary  # install libraries for postgres

Anschließend werden die settings angepasst.
Dazu die `settings.py` im `config` Verzeichnis öffnen.
Dort befindet sich eine Settings-Variable mit dem Namen `DATABASES`.
In dieser Variablen werden die Einstellungen hinterlegt.

	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.postgresql',
	        'NAME': 'database_name',
    	    'USER': 'database_username',
        	'PASSWORD': 'password',
	        'HOST': 'localhost',
    	    'PORT': '5432',
		}
	}

Im Anschluss müssen die Migrationen durchgeführt und ggf ein Admin-User angelegt weren.
