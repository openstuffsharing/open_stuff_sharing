from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, RedirectView

from .models import Advertisement, District, Message, MessageHistory, SubCategory


class Index(LoginRequiredMixin, ListView):
    queryset = Advertisement.objects.order_by('-id')[:10]
    template_name = 'oss/index.html'

    def get_context_data(self):
        context = super().get_context_data()
        context['categories'] = SubCategory.objects.filter(id__in=(
            Advertisement.objects.values_list('sub_category', flat=True)
                                 .distinct()
        )).order_by('category__name', 'name')
        context['locations'] = District.objects.filter(id__in=(
            Advertisement.objects.values_list('district', flat=True).distinct()
        )).order_by('city__name', 'name')
        return context


class AdvertisementDetail(LoginRequiredMixin, DetailView):
    queryset = Advertisement.objects.all()
    template_name = 'oss/advertisement_detail.html'


class CreateAdvertisement(LoginRequiredMixin, CreateView):
    fields = (
        'title',
        'sub_category',
        'description',
        'district',
        'image',
    )
    queryset = Advertisement.objects.all()
    success_url = '/advertisements/'
    template_name = 'oss/create_advertisement.html'

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        return super().form_valid(form)


class AdvertisementList(LoginRequiredMixin, ListView):
    def get_queryset(self):
        return Advertisement.objects.filter(user=self.request.user).order_by('-id')


class MessageHistoryRedirect(RedirectView):
    def get_redirect_url(self):
        return reverse(
            'messages-detail',
            kwargs=dict(pk=MessageHistory.objects.filter(advertisement__user=self.request.user).latest('id').pk)
        )


class MessageList(LoginRequiredMixin, DetailView):
    template_name = 'oss/message_list.html'

    def get_queryset(self):
        return MessageHistory.objects.filter(advertisement__user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['message_histories'] = self.get_queryset()
        return context
