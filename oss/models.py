from django.conf import settings
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=126)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    name = models.CharField(max_length=126)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.category.name} - {self.name}'


class City(models.Model):
    name = models.CharField(max_length=126)

    def __str__(self):
        return self.name


class District(models.Model):
    name = models.CharField(max_length=126)
    city = models.ForeignKey(City, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.city.name} - {self.name}'


class Advertisement(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    image = models.ImageField(null=True, blank=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)


class MessageHistory(models.Model):
    advertisement = models.ForeignKey(Advertisement, on_delete=models.CASCADE)
    participant = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


class Message(models.Model):
    message_history = models.ForeignKey(MessageHistory, on_delete=models.CASCADE, related_name='messages')
    sender = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
