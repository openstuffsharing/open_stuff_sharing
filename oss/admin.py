from django.contrib import admin

from . import models


admin.site.register(models.Advertisement)
admin.site.register(models.Category)
admin.site.register(models.City)
admin.site.register(models.District)
admin.site.register(models.SubCategory)
