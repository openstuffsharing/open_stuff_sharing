from django.urls import path

from . import views


urlpatterns = [
    path('advertisement/add/', views.CreateAdvertisement.as_view(), name='add-advertisement'),
    path('advertisements/<pk>/', views.AdvertisementDetail.as_view(), name='advertisement-detail'),
    path('advertisements/', views.AdvertisementList.as_view(), name='advertisement-list'),
    path('messages/<pk>/', views.MessageList.as_view(), name='messages-detail'),
    path('messages/', views.MessageHistoryRedirect.as_view(), name='messages'),
    path('', views.Index.as_view(), name='index'),
]
